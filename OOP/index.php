<?php
/* LATIHAN SOAL

require_once ('kendaraan.php');

$kendaraan = new kendaraan("mobil");
echo " Nama Kendaraan : $kendaraan->name <br>";
echo " Jumlah Roda : $kendaraan->roda <br>";
echo " Bahan Bakarnya : $kendaraan->bensin <br>";
echo "<br>";

------------------------------------------*/

// SOAL 1 OOP ----------------------------
require_once ('animal.php');
$sheep = new animal("shaun");

echo " Nama Animal : $sheep->name <br>"; // "shaun"
echo " Jumlah kaki : $sheep->legs <br>"; // 2
echo " Apakah berdarah dingin : $sheep->cold_blooded <br>"; // false

// SOAL 2 OOP-----------------------------
require_once ('frog.php');
require_once ('ape.php');

$sungokong = new Ape("kera sakti");
echo " Suara monyet : $sungokong->yell <br>"; // "Auooo"

$kodok = new Frog("buduk");
echo " Suara katak : $kodok->jump <br>" ; // "hop hop"

