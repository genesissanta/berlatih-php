<?php
function ubah_huruf($string)
{
for ($i = 0; $i < strlen($string); $i++){
	/*
		ord() untuk konversi string ke angka
		chr() untuk konversi angka ke string
	*/

	$new_string[$i] = chr(ord($string[$i])+1);
}
//implode() untuk konversi array ke string
return implode($new_string). '<br>';
}

// TEST CASES
echo ubah_huruf("wow"); // xpx
echo "<br>";
echo ubah_huruf("developer"); // efwfmpqfs
echo "<br>";
echo ubah_huruf("laravel"); // mbsbwfm
echo "<br>";
echo ubah_huruf("keren"); // lfsfo
echo "<br>";
echo ubah_huruf("semangat"); // tfnbohbu
echo "<br>";

?>