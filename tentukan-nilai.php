<?php
function tentukan_nilai($number)
{
    if ($number <= 100) {$grade = "Sangat Baik";}
    if ($number <= 84) {$grade = "Baik";}
    if ($number <= 69) {$grade = "Cukup";}
    if ($number <= 59) {$grade = "Kurang";}

    return $grade ;
    //  kode disini
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";
echo tentukan_nilai(76); //Baik
echo "<br>";
echo tentukan_nilai(67); //Cukup
echo "<br>";
echo tentukan_nilai(43); //Kurang
echo "<br>";
?>