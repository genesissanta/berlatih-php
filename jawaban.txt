1. Buat database

C:\xampp\mysql\bin                                                
? mysql -uroot -p                                                 
Enter password:                                                   
Welcome to the MariaDB monitor.  Commands end with ; or \g.       
Your MariaDB connection id is 940                                 
Server version: 10.1.38-MariaDB mariadb.org binary distribution   
                                                                  
Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and other
s.                                                                
                                                                  
Type 'help;' or '\h' for help. Type '\c' to clear the current inpu
t statement.                                                      
                                                                  
MariaDB [(none)]> create database myshop;                         
Query OK, 1 row affected (0.01 sec)                


--------------------------------------------------
Soal no 2

MariaDB [(none)]> create database myshop;
 
                                    
MariaDB [(none)]> use myshop;       
Database changed                    
MariaDB [myshop]> create table user(
    -> id int auto_increment,       
    -> name varchar(225),           
    -> email varchar(225),          
    -> password varchar(225),       
    -> primary key(id)              
    -> );                           

MariaDB [myshop]> create table items(
    -> id int auto_increment,
    -> name varchar(225),
    -> description varchar(225),
    -> price int,
    -> stock int,
    -> category_id int,
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );

MariaDB [myshop]> create table categories(
    -> id int auto_increment,
    -> name varchar(225),
    -> primary key(id)
    -> );


--------------------------------------------------
Soal no 3


insert into user values(
    -> "John Doe","john@doe.com"."john123"),
    -> "Jane Doe","jane@doe.com"."jenita123");

insert into categories values(
    -> "gadget"),
    -> "cloth"),
    -> "men"),
    -> "women"),
    -> "branded");

insert into items values(
    -> "Sumsang b50","hape keren dari merek sumsang",4000000,100,1),
    -> "Uniklooh","baju keren dari brand ternama",500000,50,2),
    -> "IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

-----------------------------------------------------
Soal no 4

a. select id,name,email from user;
b. select min(1000000) as smallestprice from items;
   select * from item where name like 'watch%';
c. select name,description,price,stock,category_id from item inner join categories on name;

-----------------------------------------------------
soal no 5

update item
set Price = 2500000
where id=1;






